const API_URL = "http://localhost:7000";

const API_PATH = {
  EXCEL : {
    SAVE : '/excel/save'
  },
  CSV : {
    SAVE : '/csv/save'
  }
}


const LANG_KO = 'langKo';
const LANG_EN = 'langEn';

let USER_MESSAGE_DATA = null;