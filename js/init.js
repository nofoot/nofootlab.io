import Router from "./Router.js";
document.addEventListener('readystatechange', () =>{
  if(document.readyState === 'complete'){}
});

const versionChecker = (versionInfo)=>{
  const wrapStyle = `color:#F7FACC; font-size:1rem; background-color: #000;`
  const versionStyle = `color:#000; font-size:0.8rem;`
  const wrapSplitLine = `==================================================`
  const messageSplitLine = '------------------------------------------------------------------';
  console.info(`%c${wrapSplitLine}`,wrapStyle);
  console.log(`%cRelease version : ${versionInfo.version}`, versionStyle);
  console.log(messageSplitLine);
  versionInfo.message.forEach((versionStr,idx)=>{
    console.log(`%c[${idx+1}] ->  ${versionStr}` , versionStyle);
  })
  console.log(messageSplitLine);
  console.log()
  console.info(`%c${wrapSplitLine}`,wrapStyle);
}


window.addEventListener('load', (event) => {

  const versionHistory = {
    version : '1.00.03',
    date : '2023.07.20 11:20',
    message : [
      //1.0.01
      // 'Page1 : 배경이미지 변경',
      // 'Page1 : 하단 실선 위치 고정 및 불투명도 100%',
      // 'Page1 : 폰트 크기 조절',
      // 'Page2 : Input 영역 width 제한을 두지 않고 element 가운데 정렬 처리',
      // 'Page2 : 폰트 크기 조절',
      // 'Page3 : 메시지 영역 가운데 정렬',
      // 'Page3 : 메시지 템플릿에 영문버전 추가',
      // 'Page3 : gif 이미지를 sprite 처리',
      // 'Page3 : 폰트 크기 조절'
      //1.00.02
      //'Page3 : 텍스트 박스 언어설정에 따라 폰트 클래스 적용'
      //1.00.03
      'Page3 : 30초 Page1으로 이동하도록 setTimeout 처리'
    ]
  }
  versionChecker(versionHistory);
  const router = new Router();
});

window.addEventListener('beforeunload', (event) => {
  // 명세에 따라 preventDefault는 호출해야하며, 기본 동작을 방지합니다.
  location.href = location.origin
  event.preventDefault();
});