export default class UserMessage {
  constructor(){
    this.name = null;
    this.message = null;
    this.lang = null;
  }
}