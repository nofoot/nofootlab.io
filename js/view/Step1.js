import UserMessage from "../data/UserMessage.js";

export default class Step1View{
  constructor(){
    this.init();
    this.inputEventHandler = (evt)=>{
      const evtTarget = evt.target;
      switch(evtTarget.id){
        case 'langKo':
        case 'langEn':
          const langIconButtons = document.querySelectorAll('div.icon-wrap > div');
          if(langIconButtons && langIconButtons?.length){
            langIconButtons.forEach(buttonElement=>{
              buttonElement.className = (buttonElement.id === evtTarget.id)? 'activate' : '';
            });
          }
  
          const inputUserNameDom = document.querySelector('div#userNameWrap');
          if(inputUserNameDom){
            const LABEL_KO = '당신의 이름은 무엇인가요?';
            const LABEL_EN = 'What’s your name?';
  
            const PLACEHOLDER_KO = '이름을 입력해주세요.';
            const PLACEHOLDER_EN = 'Please enter a name.';
  
            const inputLabel = inputUserNameDom.querySelector('label');
            const inputComp = inputUserNameDom.querySelector('input');
  
            inputLabel.innerHTML= (evtTarget.id === 'langKo')? LABEL_KO : LABEL_EN;
            inputLabel.className =  (evtTarget.id === 'langKo')? 'ko-font' : 'en-font';
  
            inputComp.placeholder = (evtTarget.id === 'langKo')? PLACEHOLDER_KO : PLACEHOLDER_EN;
            inputComp.className =  (evtTarget.id === 'langKo')? 'ko-font' : 'en-font'
  
            inputComp.focus();
            USER_MESSAGE_DATA.lang = evtTarget.id;
          }

          this.fixToUserNameCenter();
        break;
        
        case 'arrowRight':
          const userNameInput = document.querySelector('input#userName');
          if(userNameInput?.value && userNameInput.value?.length){
            USER_MESSAGE_DATA.name =  Handlebars.Utils.escapeExpression(userNameInput.value);
            try{
              this.removeEvents();
            }catch(excp){
              console.error(excp);
            }
            history.pushState(null,null, '#step2');
            window.dispatchEvent(new Event('popstate'));
          }else{
            userNameInput.focus();
          }
        break;
  
        case 'userName':
        const userNameInputElm = document.querySelector('input#userName');
        const arrowRight = document.querySelector('div#arrowRight');
        if(userNameInputElm?.value && userNameInputElm.value.length){
          arrowRight.style.display = 'block';
        }else{
          arrowRight.style.display = 'none';
        }
        break;
      }
    }
  }

  init(){

  }

  render(){
    if(!USER_MESSAGE_DATA){
      USER_MESSAGE_DATA = new UserMessage();
    }
    this.registerHelper();
    const handlebarsTpl = document.querySelector(`#step1Tpl`).innerHTML;
    const handlerTpl = Handlebars.compile(handlebarsTpl);
    const contentDiv = document.querySelector('div#content');
    contentDiv.innerHTML = handlerTpl(USER_MESSAGE_DATA);
    const arrowRight = document.querySelector('div#arrowRight');

    if(USER_MESSAGE_DATA.name?.length){
      arrowRight.style.display = 'block';
    }else{
      arrowRight.style.display = 'none';
    }

    this.setLang(USER_MESSAGE_DATA.lang);
    this.fixToUserNameCenter();
    this.addEvents();
  }

  fixToUserNameCenter(){
    const userNameWrap = document.querySelector('div#userNameWrap');
    const elemWidth = userNameWrap.offsetWidth;
    const windowWidth = window.innerWidth;
    const perWOfWin = Math.round((elemWidth/windowWidth) * 100);
    userNameWrap.style.marginLeft = `${Math.round((100 - perWOfWin) /2)}%`;
  }

  registerHelper(){
    Handlebars.registerHelper('setNameInput', function (context,option) {
      let inputLabel = '당신의 이름은 무엇인가요?';
      let inputPlaceHolder = '이름을 입력해주세요.';
      let langClass = 'ko-font';
      
      const userNameValue = (USER_MESSAGE_DATA.name && USER_MESSAGE_DATA.name?.length)? USER_MESSAGE_DATA.name : '';
      if(USER_MESSAGE_DATA.lang === LANG_EN){
        inputLabel = 'What’s your name?';
        inputPlaceHolder = 'Please enter a name.';
        langClass = 'en-font';
      }
      const labelElem = `<label class="${langClass}">${inputLabel}</label>`;
      const msgInputElem = `<input type="text" id="userName" placeholder="${inputPlaceHolder}" class="${langClass}" value="${userNameValue}" autocomplete="off" maxlength="25"/>`;

      return  new Handlebars.SafeString(labelElem + msgInputElem);
    });
  }

  setLang(selectedLang){
    const langIconButtons = document.querySelectorAll('div.icon-wrap > div');
    if(langIconButtons && langIconButtons?.length){
      langIconButtons.forEach(buttonElement=>{
        buttonElement.className = ((buttonElement.id === selectedLang) || (buttonElement.id === LANG_KO && !selectedLang ))? 'activate' : '';
      });
    }
  }

  removeEvents(){
    try{
      const langIconButtons = document.querySelectorAll('div.icon-wrap > div');
      if(langIconButtons && langIconButtons?.length){
        langIconButtons.forEach(buttonElement=>{
          buttonElement.removeEventListener('click',this.inputEventHandler);
        });
      }
  
      const arrowRightButton = document.querySelector('div#arrowRight');
      arrowRightButton.removeEventListener('click', this.inputEventHandler);
  
      const userNameInput = document.querySelector('input#userName');
      userNameInput.removeEventListener('keyup', this.inputEventHandler);
    }catch(excp){
      console.error(excp);
    }
  }

  addEvents(){
    const langIconButtons = document.querySelectorAll('div.icon-wrap > div');
    const _this = this;
    if(langIconButtons && langIconButtons?.length){
      langIconButtons.forEach(buttonElement=>{
        buttonElement.addEventListener('click',this.inputEventHandler);
      });
    }

    const arrowRightButton = document.querySelector('div#arrowRight');
    arrowRightButton.addEventListener('click', this.inputEventHandler);

    const userNameInput = document.querySelector('input#userName');
    userNameInput.addEventListener('keyup', this.inputEventHandler);
    userNameInput.focus();
  }
}