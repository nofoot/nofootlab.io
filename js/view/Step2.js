export default class Step2View{
  constructor(){
    this.init();
  }

  init(){
    this.inputEventHandler = (evt)=>{
      const targetId = evt.target.id;
      switch(targetId){
        case 'arrowLeft':
          history.pushState(null,null, '#step1');
          window.dispatchEvent(new Event('popstate'));
        break;
  
        case 'arrowRight':
          const userMessageInput = document.querySelector('input#userMessage');
          USER_MESSAGE_DATA.message = Handlebars.Utils.escapeExpression( userMessageInput.value);
          this.removeEvent();
          history.pushState(null,null, '#step3');
          window.dispatchEvent(new Event('popstate'));
        break;
  
        case 'userMessage':
          const userNameInputElm = document.querySelector('input#userMessage');
          const arrowRight = document.querySelector('div#arrowRight');
          if(userNameInputElm?.value && userNameInputElm.value.length){
            arrowRight.style.display = 'block';
          }else{
            arrowRight.style.display = 'none';
          }
        break;
      }
    }
  }

  render(){
    this.registerHelper();
    const handlebarsTpl = document.querySelector(`#step2Tpl`).innerHTML;
    const handlerTpl = Handlebars.compile(handlebarsTpl);
    const contentDiv = document.querySelector('div#content');
    contentDiv.innerHTML = handlerTpl(null);

    const arrowRight = document.querySelector('div#arrowRight');
    if(USER_MESSAGE_DATA.message?.length){
      arrowRight.style.display = 'block';
    }else{
      arrowRight.style.display = 'none';
    }

    this.fixToUserMessageCenter();
    this.addEvent();
  }

  fixToUserMessageCenter(){
    const userNameWrap = document.querySelector('div#userMessageWrap');
    const elemWidth = userNameWrap.offsetWidth;
    const windowWidth = window.innerWidth;
    const perWOfWin = Math.round((elemWidth/windowWidth) * 100);
    userNameWrap.style.marginLeft = `${Math.round((100 - perWOfWin) /2)}%`;
  }

  registerHelper(){
    Handlebars.registerHelper('setMsgInput', function (context,option) {
      let inputLabel = '새소년에게 하고 싶은 말을 남겨주세요. (150자 이하)';
      let langClass = 'ko-font';
      let wrapClass = 'wrapKo'
      
      if(USER_MESSAGE_DATA.lang === LANG_EN){
        inputLabel = 'Leave a message for SE SO NEON.(within 150 characters)';
        langClass = 'en-font';
        wrapClass = 'wrapEn';
      }
      const labelElem = `<label class="${langClass}">${inputLabel}</label>`;
      const msgInputElem = `<input type="text" id="userMessage" class="${langClass}" autocomplete="off" maxlength="150"/>`;

      return  new Handlebars.SafeString(`<div id="userMessageWrap" class="${wrapClass}">${labelElem}${msgInputElem}</div>`);
    });
  }

  addEvent(){
    const arrowLeftButton = document.querySelector('div#arrowLeft');
    arrowLeftButton.addEventListener('click', this.inputEventHandler);

    const arrowRightButton = document.querySelector('div#arrowRight');
    arrowRightButton.addEventListener('click', this.inputEventHandler);

    const userMessageInput = document.querySelector('input#userMessage');
    userMessageInput.addEventListener('keyup', this.inputEventHandler);
    userMessageInput.focus();
  }

  removeEvent(){
    const arrowLeftButton = document.querySelector('div#arrowLeft');
    arrowLeftButton.removeEventListener('click', this.inputEventHandler);

    const arrowRightButton = document.querySelector('div#arrowRight');
    arrowRightButton.removeEventListener('click', this.inputEventHandler);

    const userMessageInput = document.querySelector('input#userMessage');
    userMessageInput.removeEventListener('keyup', this.inputEventHandler);
  }
}